#!/usr/bin/python2

import os
import pdb
from datetime import datetime
import time
import sys
from os import path
from syslog import syslog
import xmpp
from ConfigParser import SafeConfigParser

def timestamp(d):
    diff = d - datetime(1970,1,1)
    return diff.total_seconds()

def get_opt(config, section, prop, defvalue = ""):
    try:
        return config.get(section, prop, defvalue).strip()
    except Exception as e:
        return defvalue

def log(msg):
    syslog(msg)
    print(msg)

basepath = "{}/.local/share/alive-checker".format(os.environ.get('HOME'))
config = SafeConfigParser(allow_no_value = True)
config.read("{}/config.ini".format(basepath))

path = get_opt(config, 'paths', 'check_file_path', "{}/status.txt".format(basepath))
last_alert_path = get_opt(config, 'paths', 'last_alert_path', "{}/last_alert.txt".format(basepath))

poll_time = int(get_opt(config, 'intervals', 'poll_time', 3600))
alive_time = int(get_opt(config, 'intervals', 'alive_time', 86400))
alert_time = int(get_opt(config, 'intervals', 'alert_time', 3600))
polling = bool(get_opt(config, 'mode', 'polling', False))

username = get_opt(config, 'credentials', 'username')
password = get_opt(config, 'credentials', 'password')
hostname = get_opt(config, 'credentials', 'hostname')
port = int(get_opt(config, 'credentials', 'port', 5222))
resource_name = get_opt(config, 'credentials', 'resource', hostname + "-" + username)
rcpt = get_opt(config, 'notification', 'rcpt')

log("Check file path: {}".format(path))
log("Last alert file path: {}".format(last_alert_path))

log("Username: {}".format(username))
log("Hostname: {}".format(hostname))
log("Port: {}".format(port))

def send_msg(rcpt, msg):
    client = xmpp.Client(hostname)
    client.connect((hostname, port))
    client.auth(username, password, resource_name, 1)
    client.sendInitPresence()

    msg = xmpp.Message(rcpt, msg)
    msg.setAttr('type', 'chat')
    client.send(msg)

def notify(msg):
    log("Notify: {}".format(msg))
    send_msg(rcpt, msg)
    
def get_last_alert():
    try:
        with open(last_alert_path, 'r') as f:
            data = f.read()
            return float(data)
    except Exception as e:
        print(e)
        return 0

def save_alert(alert_timestamp):
    with open(last_alert_path, 'w+') as f:
        f.write(str(alert_timestamp))

def poll():
    stats = os.stat(path)
    n = datetime.now()

    diff_seconds = (n - datetime.fromtimestamp(stats.st_mtime)).total_seconds()
    log("Diff seconds: {}".format(diff_seconds))
    if diff_seconds > alive_time:
        last_alert = get_last_alert()
        if timestamp(n) - last_alert > alert_time:
            notify("[{}]: ALERT!!!! There is no contact from the operator"
                    .format(n))
            save_alert(timestamp(n))
    else:
        log("No alert. Last access:{}"
                .format(datetime.fromtimestamp(stats.st_mtime)))


# initial files creating

if not(os.path.exists(path)):
    log('No check file. Recreating and exiting...')
    notify("No check file. Recreating and exiting...")

    # we are creating the new check file
    log("Basepath: {}".format(basepath))
    if not(os.path.exists(basepath)):
        os.makedirs(basepath)
    with open(path, 'w+') as f:
        f.write("CHECK FILE")

    sys.exit(0)

# main polling place

if polling:
    while True:
        poll()
        time.sleep(poll_time)
else:
    poll()

